package bamTrackLib;
our $VERSION = '0.001_002';
$VERSION = eval $VERSION;

# wkretzsch@gmail.com          19 Jul 2013

use 5.010;
use strict;
use warnings;
use Carp;
use Data::Dumper;
use DBI;
use File::Basename;
use Digest::MD5;

use Moose;
use namespace::autoclean;
use List::MoreUtils qw/any/;

#use DM;
use Cwd 'abs_path';
use File::Path qw(make_path remove_tree);
use FileHandle;
use URI;
use Sys::Hostname;
use IPC::Open3;
use Clone qw(clone);
use Net::SSH::Perl;

has operations => ( is => 'ro', isa => 'ArrayRef[Str]' );
has inputFile  => ( is => 'ro', isa => 'Str', );
has dbHandle   => ( is => 'ro', isa => 'DBI::db', writer => '_dbHandle' );

has db   => ( is => 'ro', isa => 'Str', required => 1 );
has host => ( is => 'ro', isa => 'Str', required => 1 );
has user => ( is => 'ro', isa => 'Str', required => 1 );
has _tables   => ( is => 'rw', isa => 'HashRef[Str]' );
has _headers  => ( is => 'rw', isa => 'HashRef[Str]' );
has inputBams => ( is => 'ro', isa => 'HashRef[URI]', writer => '_inputBams' );
has inputBamsAsLocal =>
  ( is => 'ro', isa => 'ArrayRef[Str]', writer => '_inputBamsAsLocal' );
has inputBamsByHost =>
  ( is => 'ro', isa => 'HashRef[HashRef[URI]]', writer => '_inputBamsByHost' );
has inputBamMD5sumFiles =>
  ( is => 'ro', isa => 'HashRef[URI]', writer => '_inputBamMD5sumFiles' );
has inputBamMD5sumFilesByHost => (
    is     => 'ro',
    isa    => 'HashRef[HashRef[URI]]',
    writer => '_inputBamMD5sumFilesByHost'
);
has inputBamMD5sums =>
  ( is => 'ro', isa => 'HashRef[Str]', writer => '_inputBamMD5sums' );
has _inputBamsRecalibrated => ( is => 'rw', isa => 'Bool', default => undef );
has _ctx   => ( is => 'rw', isa => 'Digest::MD5' );
has tmpDir => ( is => 'rw', isa => 'Str', default => '/tmp/bamTrack' );
has DEBUG  => ( is => 'rw', isa => 'Bool', default => 1 );
has _FHDEBUG =>
  ( is => 'rw', isa => 'FileHandle', builder => '_build_FHDEBUG' );

has _sshState =>
  ( is => 'rw', isa => 'HashRef[HashRef[Bool]]', builder => '_build_sshState' );
has _sshObject => (
    is      => 'rw',
    isa     => 'HashRef[Net::SSH::Perl]',
    builder => '_build_sshObject'
);

has returnAs => ( is => 'rw', isa => 'Str', default => 'absolute' );

has _returnURIs => ( is => 'rw', isa => 'ArrayRef[URI]' );

has _validReturnTypes => (
    is      => 'ro',
    isa     => 'ArrayRef[Str]',
    builder => '_build_validReturnTypes',
);

before '_inputBamsRecalibrated' => sub {

    my $self  = shift;
    my $input = shift;
    if ( defined $input && $input ) {

        my @inputBams = sort keys %{ $self->inputBams };
        my @nonRecal = grep { $_ !~ m/recalibrated/ } @inputBams;
        croak
"Input bams are not all recalibrated. Please register recalibrated and non-recalibrated bams separately. Following is a list of non-recalibrated input bams:\n"
          . join( "\n", @nonRecal ) . "\n"
          if @nonRecal;
    }
};

sub _build_validReturnTypes {
    return [qw/absolute rsyncURI rsyncShort/];
}

around _returnURIs => sub {
    my $orig = shift;
    my $self = shift;

    if (@_) {
        $self->$orig( \@_ );
    }
    my @returnURIs = @{ $self->$orig };
    return @{ $self->_convertURI( \@returnURIs, $self->returnAs ) };
};

around returnAs => sub {
    my $orig = shift;
    my $self = shift;

    if (@_) {
        my $returnAs         = shift;
        my @validReturnTypes = @{ $self->_validReturnTypes };

        croak
"invalid return specifier $returnAs.  Valid specifiers are: @validReturnTypes"
          unless any { $returnAs eq $_ } @validReturnTypes;
        $self->$orig($returnAs);
    }
    return $self->$orig;
};

sub _convertURI {

    my $self = shift;
    my ( $returnURIs, $returnAs ) = @_;
    my @returnURIs = @{$returnURIs};
    my @return;
    given ($returnAs) {
        when ('rsyncURI') { @return = @returnURIs }
        when ('rsyncShort') {
            @return = map { $_->host . ':' . $_->path } @returnURIs
        }
        when ('absolute') {
            @return = map { $_->path } @returnURIs
        }
        default {
            croak
"programming error: unknown returnAs, should have been caught earlier"
        }
    }
    return \@return;
}

sub _build_sshState {
    my $self      = shift;
    my $localhost = hostname;
    $self->_sshState( { $localhost => { loggedIn => 1, localHost => 1 } } );
}

sub _build_sshObject {
    my $self = shift;
    $self->_sshObject( {} );
}

sub _build_FHDEBUG {
    my $self = shift;
    my $retval;
    if ( $self->DEBUG ) {
        $retval = FileHandle->new('>&STDERR');
    }
    else {
        $retval = FileHandle->new('>/dev/null');
    }
    return $retval;
}

around DEBUG => sub {
    my $orig = shift;
    my $self = shift;

    if (@_) {
        my $arg = shift;
        $self->$orig($arg);
        if ($arg) {
            $self->_FHDEBUG( FileHandle->new('>&STDERR') );
        }
        else {
            $self->_FHDEBUG( FileHandle->new('>/dev/null') );
        }
    }
    return $self->$orig;
};

around _inputBams => sub {
    my $orig = shift;
    my $self = shift;
    my %args = @_;

    # parse bam list
    my @inputBams;
    croak "type must be defined" unless defined $args{type};
    if ( $args{type} eq 'fileList' ) {
        push @inputBams, @{ $self->_parseBamListFile( $args{input} ) };
    }
    elsif ( $args{type} eq 'file' ) {
        push @inputBams, $args{input};
    }
    elsif ( $args{type} eq 'files' ) {
        croak "input of type files needs to be an array ref"
          unless ref( $args{input} ) eq 'ARRAY';
        push @inputBams, @{ $args{input} };
    }
    croak "no input files given" unless @inputBams;

    my %uriByHost = %{ $self->_convertToURI( paths => \@inputBams ) };

    # serialize uris and test they exist
    my @inputURIs;
    for my $host ( sort keys %uriByHost ) {
        $self->_sshLogin($host);
        for my $uriKey ( sort keys %{ $uriByHost{$host} } ) {
            my $uri = $uriByHost{$host}->{$uriKey};
            push @inputURIs, $uri;

            # test bams exist
            my ( $stdout, $stderr, $exit ) =
              $self->_sshCommand( "test -e " . $uri->path, host => $host );
            croak "Bam does not exist: " . $uri->as_string if $exit;

        }
    }
    my %inputURIs = map { $_->as_string => $_ } @inputURIs;

    # store input bams
    $self->$orig( \%inputURIs );
    $self->_inputBamsAsLocal( [ map { $_->path } @inputURIs ] );
    $self->_inputBamsByHost( \%uriByHost );

    ### update md5sums based on those input bams
    ## look for md5sums based on bamList names
    # find md5sums not existent
    # run md5sums on any files that still need it
    my %inputBamMD5sumFiles       = %{ clone( \%inputURIs ) };
    my %inputBamMD5sumFilesByHost = %{ clone( \%uriByHost ) };
    my $fhDebug                   = $self->_FHDEBUG;

    for my $host ( sort keys %uriByHost ) {
        $self->_sshLogin($host);

        my %hostURIs = %{ $uriByHost{$host} };
        for my $bamURIkey ( sort keys %hostURIs ) {
            my $bamURI = $inputURIs{$bamURIkey};
            my $md5URI = $bamURI->clone;
            $md5URI->path( $bamURI->path . '.md5' );

            print $fhDebug "creating missing md5sums: ". $md5URI->as_string."\n";

            my $md5Path = $md5URI->path;
            my $bamPath = $bamURI->path;
            my $bamDir  = dirname($bamPath);
            my $bamBase = basename($bamPath);
            my ( $stdout, $stderr, $exit ) =
              $self->_sshCommand(
                "test -e $md5Path && test $md5Path -nt $bamPath",
                host => $host );

            ( $stdout, $stderr, $exit ) =
              $self->_sshCommand( "cd $bamDir && md5sum $bamBase > $md5Path",
                host => $host )
              if $exit;

            croak "could not create md5sum of $bamPath on host $host: $stderr"
              if $exit;

            $inputBamMD5sumFilesByHost{$host}->{$bamURIkey} = $md5URI;
            $inputBamMD5sumFiles{$bamURIkey} = $md5URI;
        }
    }

    $self->_inputBamMD5sumFiles( \%inputBamMD5sumFiles );
    $self->_inputBamMD5sumFilesByHost( \%inputBamMD5sumFilesByHost );

    # open md5sum files to store their m5sums in database
    my %md5sums = %inputBamMD5sumFiles;
    for my $host ( sort keys %inputBamMD5sumFilesByHost ) {
        my %hostHash = %{ $inputBamMD5sumFilesByHost{$host} };
        $self->_sshLogin($host);
        for my $bam ( sort keys %hostHash ) {
            my $md5URI = $hostHash{$bam};
            my ( $stdout, $stderr, $exit ) =
              $self->_sshCommand( "cat " . $md5URI->path, host => $host );
            croak "error trying to access $md5URI->path(): $stderr" if $exit;

            chomp $stdout;
            $stdout =~ m/^([0-9a-f]+)/;
            my $md5sum = $1;
            croak
"md5sum of file $md5URI->path() on host $host is malformed: length of md5sum is not 32 characters"
              unless length $md5sum == 32;

            $md5sums{$bam} = $md5sum;
        }
    }

    $self->_inputBamMD5sums( \%md5sums );

    return \%inputURIs;
};

around _ctx => sub {
    my $orig = shift;
    my $self = shift;

    return $self->$orig(@_) if @_;

    my $ctx = $self->$orig;
    unless ( defined $ctx ) {
        $ctx = Digest::MD5->new;
        $self->$orig($ctx);
    }
    return $self->$orig;
};

sub BUILD {
    my $self = shift;

    croak
"Please export your password to DBI_PASS environment var before running this script"
      unless defined $ENV{DBI_PASS};

    make_path( $self->tmpDir );

    $self->connect;
    my $dbh = $self->dbHandle;

   #    my $sth = $dbh->prepare('SHOW VARIABLES LIKE "%version%"');
   #    $sth->execute();
   #    while ( my @row = $sth->fetchrow_array ) {
   #    print "@row\n";
   #  }
   #exit;
   #    print STDERR "Using mysql server version $dbh->{mysql_serverversion}\n";

    my %tables;

    # create required tables if they do not exist
    my $table        = 'md5sums';
    my $stringLength = 255;
    my $md5sumLength = 32;
    my $tableCreateCommand =
"CREATE TABLE IF NOT EXISTS $table ( md5sum VARCHAR($md5sumLength) PRIMARY KEY, sampleName VARCHAR($stringLength), sampleNum INT, passedValidateSam BOOL NOT NULL DEFAULT 0, recalibrated BOOL);";
    $dbh->do($tableCreateCommand);
    $tables{$table} = 1;

    $table = 'bamNames';
    $tableCreateCommand =
"CREATE TABLE IF NOT EXISTS $table ( md5sum VARCHAR($md5sumLength) NOT NULL, bamName VARCHAR($stringLength) NOT NULL, bamURI VARCHAR($stringLength) not null, backup BOOL not null, backupDeviceName VARCHAR($stringLength) not null, rowMD5 VARCHAR($md5sumLength) PRIMARY KEY);";
    $dbh->do($tableCreateCommand);
    $tables{$table} = 1;

    $self->_tables( \%tables );
    my %headers;
    $headers{$_} = 1
      for
      qw/md5sum sampleName sampleNum passedValidateSam recalibrated bamName bamURI backup backupDeviceName rowMD5/;
    $self->_headers( \%headers );

    $dbh->commit;

}

sub DEMOLISH {
    my $self = shift;
    my $dbh  = $self->dbHandle();
    $dbh->disconnect;
}

sub connect {
    my $self = shift;
    unless ( defined $self->dbHandle ) {
        my $DBI =
          DBI->connect( 'dbi:mysql:' . $self->db() . ':' . $self->host(),
            $self->user(), undef, { RaiseError => 1, AutoCommit => 0 } )
          || croak "Error opening database: $DBI::errstr";
        $self->_dbHandle($DBI);
    }
    return 1;
}

sub _convertToURI {
    my $self      = shift;
    my %args      = @_;
    my @bams      = @{ $args{paths} };
    my $isDir     = $args{isDir} || 0;
    my $createDir = $args{createDir} || 0;

    my %uriByHost;
    for my $bam (@bams) {
        my $uri;

        # parse as rsync uri if this is an rsync uri
        if ( $bam =~ m/^rsync:/ ) {
            $uri = URI->new($bam);
        }

        # parse the other rsync way of specifying sources
        # i.e. [user@]host:destination
        elsif ( $bam =~ m/:/ ) {
            my ( $host, $path ) = split( /:/, $bam );
            $uri = URI->new();
            $uri->scheme('rsync');
            $uri->host($host);
            $uri->path($path);
        }

        # parse as absolute path, get hostname from system
        else {
            # save bam in uri
            $uri = URI->new();
            $uri->scheme('rsync');
            my $host = hostname;
            $uri->host($host);
            $uri->path($bam);

        }
        croak "no path defined for bam $bam" unless defined $uri->path;
        croak "path of $bam is not absolute" unless $uri->path =~ m|^/|;
        croak "programming error: scheme is not rsync"
          unless $uri->scheme eq 'rsync';
        croak "programming error: host is not defined"
          unless defined $uri->host;

        $uriByHost{ $uri->host } = {} unless exists $uriByHost{ $uri->host };
        $uriByHost{ $uri->host }->{ $uri->as_string } = $uri;
    }

    # assume paths are reachable
    # reach paths and then get absolute path
    my $localHost = hostname;
    for my $host ( sort keys %uriByHost ) {

        $self->_sshLogin($host);

        my %hostUris = %{ $uriByHost{$host} };
        for my $uriKey ( sort keys %hostUris ) {
            my $uri     = $hostUris{$uriKey};
            my $uriPath = $uri->path;
            my ( $stdout, $stderr, $exit );

            # create dirs if they don't exist
            if ( $isDir && $createDir ) {
                ( $stdout, $stderr, $exit ) =
                  $self->_sshCommand(
                    "test -e $uriPath" . " || mkdir -p $uriPath",
                    host => $host );
                croak "path could not be created: host:path "
                  . $uri->host() . ":"
                  . $uriPath
                  if $exit;
            }

            # test if file exists
            ( $stdout, $stderr, $exit ) =
              $self->_sshCommand( "test -e " . $uriPath, host => $host );
            croak "path does not exist on host:path "
              . $uri->host() . ":"
              . $uriPath
              if $exit;

            # create dir to then find absolute path for
            ( $stdout, $stderr, $exit ) = $self->_sshCommand(
q/perl -MCwd -e '$D = Cwd::abs_path($ARGV[0]) || exit 1; print $D . "\n" || exit 2; exit 0;' /
                  . $uriPath,
                host => $host
            );

            croak "error canonicalizing paths: $stderr" if $exit;
            croak "error in creating absolute path on remote host of uri: "
              . $uri->as_string
              . "\nabsolute path creation exited with exit status $exit"
              unless defined $stdout;

            chomp $stdout;
            $uri->path($stdout);

        }
    }

    return \%uriByHost;

}

sub _sshLogin {
    my $self = shift;
    my $host = shift;
    my @args = @_;

    my %sshState = %{ $self->_sshState };
    if ( $host eq hostname ) {
        $sshState{$host} = { loggedIn => 1, localHost => 1 };
    }
    else {
        unless ( exists $sshState{$host} && $sshState{$host}->{loggedIn} ) {

            # create new ssh connection
            # register its state and save the new ssh object
            my $ssh = Net::SSH::Perl->new( $host, debug => 0 );
            $ssh->login() or croak "error logging in to host $host";
            $sshState{$host} = { loggedIn => 1, localHost => 0 };

            my %sshObject = %{ $self->_sshObject };
            $sshObject{$host} = $ssh;
            $self->_sshObject( \%sshObject );
        }
    }

    return $self->_sshState( \%sshState );
}

sub _sshCommand {
    my $self    = shift;
    my $command = shift;
    my %args    = @_;
    my $timeout = $args{timeout};
    my $host    = $args{host} || hostname;
    my ( $exit, $out, $err );

    croak "ssh: host $host has not been logged into yet"
      unless exists $self->_sshState->{$host};

    if ( $self->_sshState->{$host}->{localHost} ) {

        my ( $wtr, $rdr, $stderr );
        use Symbol 'gensym';
        $stderr = gensym;
        my $pid = open3( $wtr, $rdr, $stderr, $command );

        waitpid( $pid, 0 );

        $exit = $? >> 8;
        $out  = do { local $/; <$rdr> || '' };
        $err  = do { local $/; <$stderr> || '' };
    }
    else {
        croak "programming error: ssh not logged in"
          unless $self->_sshState->{$host}->{loggedIn};

        my $ssh = $self->_sshObject->{$host};
        ( $out, $err, $exit ) = $ssh->cmd($command);
        $out = q// unless defined $out;
        $err = q// unless defined $err;
    }

    return ( $out, $err, $exit );
}

sub retrieveBams {
    my $self = shift;

    my %args = @_;
    $self->returnAs( $args{returnAs} ) if $args{returnAs};

    my %headers       = %{ $self->_headers };
    my %filterColumns = %{ $args{filterColumns} };
    my $returnAs      = $self->returnAs;
    my %searchHeaders = %filterColumns;

    # we are always searching for bamURIs
    $searchHeaders{bamURI} = 1;

    # save index in array of each header in searchHeaders
    my $colNum = 0;
    map { $searchHeaders{$_} = $colNum++ } sort keys %searchHeaders;

    #    print join( ' ', keys %searchHeaders ) . "\n";

    # make sure each search header exists
    for my $key ( keys %searchHeaders ) {
        croak "unknown header supplied to retrieveBams: '$key'"
          if !exists $headers{$key};
    }

    #
    my $dbh = $self->dbHandle;

    # just get all bams and parse in perl
    # lazy, I know but we'll sort it out later
    my $sth =
      $dbh->prepare( "SELECT "
          . join( ',', sort keys %searchHeaders )
          . " FROM md5sums a INNER JOIN bamNames b USING (md5sum);" );
    $sth->execute or croak $sth->errstr;

    my @returnBams;
    while ( my @row = $sth->fetchrow_array ) {

        # decide whether to keep row
        my $keep = 1;
        for my $header ( sort keys %filterColumns ) {
            my $index = $searchHeaders{$header};
            if ( ref( $filterColumns{$header} ) eq 'ARRAY' ) {
                croak "filter columns array for header $header is empty"
                  unless @{ $filterColumns{$header} };

                croak
"a value in the filterColumns array for header $header was undef"
                  if any { !defined $_ } @{ $filterColumns{$header} };

                ( $keep = 0 && last )
                  unless any { $row[$index] eq $_ }
                @{ $filterColumns{$header} };
            }
            else {
                ( $keep = 0 && last )
                  unless defined $row[$index]
                  && $row[$index] eq $filterColumns{$header};
            }
        }

        push @returnBams, $row[ $searchHeaders{bamURI} ] if $keep;
    }

    # parse uris
    my @returnURIs = map { URI->new( $_, 'rsync' ) } @returnBams;
    return $self->_returnURIs(@returnURIs);
}

sub validateBams {
    my $self = shift;
    my %args = @_;
    $self->_processInputBams(@_);

    my %md5sums   = %{ $self->inputBamMD5sums };
    my %URIByHost = %{ $self->inputBamsByHost };

    # get md5 digest of every bam file and compare to what we have on file
    my @mismatchingUris;
    for my $host ( sort keys %URIByHost ) {
        my $hostURIs = $URIByHost{$host};

        $self->_sshLogin($host);
        for my $uriKey ( sort keys %{$hostURIs} ) {

            my $uri = $hostURIs->{$uriKey};

            # do an md5 on the bam
            my ( $stdout, $stderr, $exit ) =
              $self->_sshCommand( q/md5sum / . $uri->path, host => $host );
            croak "error running md5sum on path " . $uri->path . ": $stderr"
              if $exit;
            chomp $stdout;
            $stdout =~ m/^([0-9a-f]+)/;
            my $digest = $1;

            if ( $digest ne $md5sums{$uriKey} ) {
                carp "Bam "
                  . $uri->path
                  . " does not match md5sum on record (GOT: $digest; EXPECTED: $md5sums{$uriKey})";
                push @mismatchingUris, $uri;
            }
        }
    }
    return $self->_returnURIs(@mismatchingUris);
}

sub _getDigest {
    my $self = shift;
    my $bam  = shift;

    my $ctx = $self->_ctx;

    open( my $fh, '<', $bam ) or croak "Could not open $bam";
    $ctx->addfile($fh);
    my $digest = $ctx->hexdigest;
    close($fh);

    return $digest;
}

# takes a list of bams (file or arrayref) and saves them into the DB
sub registerBams {
    my $self = shift;
    my %args = @_;

    $self->_processInputBams(@_);
    $self->_inputBamsRecalibrated( $args{recalibrated} );

    # save input bam list and create missing md5sums
    my %inputURIsByHost  = %{ $self->inputBamsByHost };
    my %md5sumURIsByHost = %{ $self->inputBamMD5sumFilesByHost };
    my %inputURIs        = %{ $self->inputBams };
    my %md5sumURIs       = %{ $self->inputBamMD5sumFiles };

    # make sure same bam files exist in both hashes
    my $areEqual = ( keys %inputURIs == keys %md5sumURIs )
      && 0 == grep { !exists $md5sumURIs{$_} } keys %inputURIs;

    croak "programming error. list of bams and md5sumFiles don't match."
      unless $areEqual;

    # pull out only name from inputBamFiles
    my @bamURIPaths = map { $inputURIs{$_}->path } sort keys %inputURIs;
    my @bamNames    = map { basename($_) } @bamURIPaths;

    # pull md5sums from files
    my %md5sums = %{ $self->inputBamMD5sums };
    my @md5sums = map { $md5sums{$_} } sort keys %md5sums;
    my $recalibrated = $self->_inputBamsRecalibrated;
    my @recalibrated = ($recalibrated) x @md5sums;

    # pull sample names from files
    my %sampleNames;
    my %sampleNums;
    for my $host ( sort keys %inputURIsByHost ) {
        my %hostURIHash = %{ $inputURIsByHost{$host} };
        $self->_sshLogin($host);
        for my $bamURIKey ( sort keys %hostURIHash ) {
            my $uriPath = $hostURIHash{$bamURIKey}->path;
            croak "error: uriPath $uriPath does not end in .bam"
              unless $uriPath =~ m/\.bam$/;

            my ( $stdout, $stderr, $exit ) =
              $self->_sshCommand(
                "samtools view -H $uriPath | grep '^\@RG' |head -1",
                host => $host );
            croak "could not read bam $bamURIKey: $stderr" if $exit;
            chomp $stdout;
            my $rgHeader = $stdout;
            $rgHeader =~ m/\tSM:([A-Za-z0-9_]+)\t/;
            my $sm = $1;
            $sampleNames{$bamURIKey} = $sm;
            $sm =~ m/(\d+)$/;
            my $sn = $1;
            $sampleNums{$bamURIKey} = $sn;
        }
    }

    my @bamURIKeys  = sort keys %inputURIs;
    my @sampleNames = map { $sampleNames{$_} } @bamURIKeys;
    my @sampleNums  = map { $sampleNums{$_} } @bamURIKeys;

    # keep track of backup flag
    my @backup = map { exists $args{backup} && $args{backup} } @bamURIPaths;
    croak "need to define backup Device name"
      if exists $args{backup}
      && $args{backup}
      && !defined $args{backupDeviceName};

    my @backupDevice =
      map {
        ( exists $args{backupDeviceName} && $args{backup} )
          ? $args{backupDeviceName}
          : 0
      } @bamURIKeys;

    my @columns =
      ( \@md5sums, \@bamNames, \@bamURIKeys, \@backup, \@backupDevice );

    my @rowMD5;
    my $ctx = $self->_ctx;
    for my $rowNum ( 0 .. $#bamURIKeys ) {
        for my $raCol (@columns) {
            $ctx->add( $raCol->[$rowNum] );
        }
        push @rowMD5, $ctx->hexdigest;
    }
    push @columns, \@rowMD5;

    my @headers = qw/md5sum bamName bamURI backup backupDeviceName rowMD5/;
    $self->_insertInTable(
        table   => 'bamNames',
        columns => \@columns,
        headers => \@headers,
    );

    $self->_insertInTable(
        table   => 'md5sums',
        columns => [ \@md5sums, \@sampleNums, \@sampleNames, \@recalibrated ],
        headers => [qw/md5sum sampleNum sampleName recalibrated/],
    );

    # look for validation files
    my $index = 0;
    my %md5ToUpdate;
    my %validated;
    for my $host ( sort keys %inputURIsByHost ) {
        my %hostURIHash = %{ $inputURIsByHost{$host} };
        $self->_sshLogin($host);
        for my $bamURIKey ( sort keys %hostURIHash ) {
            my $uriPath = $hostURIHash{$bamURIKey}->path;
            my ( $stdout, $stderr, $exit ) =
              $self->_sshCommand( "test $uriPath.ok -nt $uriPath",
                host => $host );
            unless ($exit) {
                $validated{$bamURIKey}   = 1;
                $md5ToUpdate{$bamURIKey} = $md5sums{$bamURIKey};
            }
            $index++;
        }
    }

    my @validated       = map { $validated{$_} } sort keys %validated;
    my @md5sumsToUpdate = map { $md5ToUpdate{$_} } sort keys %validated;
    $self->_updateTable(
        table   => 'md5sums',
        columns => [ \@md5sumsToUpdate, \@validated ],
        headers => [qw/md5sum passedValidateSam/],
    );

    return @{ $self->inputBamsAsLocal };

}

sub _tableInputValidation {

    my $self   = shift;
    my %args   = @_;
    my %tables = %{ $self->_tables };
    my $table =
      defined $args{table}
      ? $args{table}
      : croak "programming error: need to define input table";

    croak "programming error: table $table is not one of known tables: "
      . join( ' ', sort keys %tables )
      unless exists $tables{ $args{table} };

    my @raColumns =
      ref( $args{columns} ) eq 'ARRAY'
      ? @{ $args{columns} }
      : croak "programming error: columns needs to be an array ref";
    my @headers =
      ref( $args{headers} ) eq 'ARRAY'
      ? @{ $args{headers} }
      : croak "programming error: headers needs to be an array ref";

    scalar @headers == scalar @raColumns
      or croak
      "programming error: number of columns must match number of headers";

    # make sure every column has same length
    my $nRows = undef;
    for my $rCol (@raColumns) {
        unless ( defined $nRows ) {
            $nRows = @{$rCol};
        }
        croak "programming error: not all columns are same length"
          unless $nRows == @{$rCol};
    }
    return ( $table, \@raColumns, \@headers );
}

sub _updateTable {
    my $self = shift;
    my %args = @_;
    my ( $table, $raColumns, $headers ) = $self->_tableInputValidation(@_);
    my @raColumns  = @{$raColumns};
    my @headers    = @{$headers};
    my $primaryID  = shift @headers;
    my $primaryCol = shift @raColumns;

    # get ready to transmit
    my $dbh = $self->dbHandle;

    # assume table is created

    # update rows
    my $cmd = "UPDATE $table SET ";
    my @headerRequests = map { "$_=?" } @headers;
    $cmd .= join( ',', @headerRequests );
    $cmd .= " WHERE $primaryID = ?";

    my $sth = $dbh->prepare($cmd);

    for my $rowNum ( 0 .. $#{ $raColumns[0] } + 1 ) {
        my @row;
        for my $raCol ( @raColumns, $primaryCol ) {
            push @row, $raCol->[$rowNum];
        }
        eval { $sth->execute(@row) };
        croak $sth->errstr . "\n SQL command: $cmd\n" if $@;
    }

    $dbh->commit;

}

sub _insertInTable {
    my $self = shift;
    my %args = @_;
    my ( $table, $raColumns, $headers ) = $self->_tableInputValidation(@_);
    my @raColumns = @{$raColumns};
    my @headers   = @{$headers};

    # get ready to transmit
    my $dbh = $self->dbHandle;

    # assume table is created

    # insert rows
    my $cmd =
        "INSERT INTO $table("
      . join( ',', @headers ) . ")"
      . " VALUES ("
      . join( ',', map { '?' } @headers ) . ")";

    # add in on duplicate update
    $cmd .= " ON DUPLICATE KEY UPDATE $headers[0] = $headers[0]";

    my $sth = $dbh->prepare($cmd);

    for my $rowNum ( 0 .. $#{ $raColumns[0] } ) {
        my @row;
        for my $raCol (@raColumns) {
            push @row, $raCol->[$rowNum];
        }
        eval { $sth->execute(@row) };
        croak $sth->errstr . "\n SQL command: $cmd\n" if $@;
    }

    # commit changes
    $dbh->commit;

    return 1;
}

sub _parseBamListFile {
    my $self = shift;
    my $file = shift;
    open( my $fh, '<', $file ) or croak "Could not open $file";
    my @bams;
    my $line;
    while (<$fh>) {
        chomp;
        $line++;
        croak
          "File on line $line of bamlist $file does not look like a bam file"
          unless m/\.bam$/;
        push @bams, $_;
    }
    return \@bams;
}

sub _processInputBams {

    my $self = shift;
    my %args = @_;

    if ( exists $args{fileList} ) {
        $self->_inputBams(
            type  => 'fileList',
            input => $args{fileList}
        );
    }
    if ( exists $args{file} ) {
        $self->_inputBams( type => 'file', input => $args{file} );
    }
    if ( exists $args{files} ) {
        $self->_inputBams( type => 'files', input => $args{files} );
    }

}

sub dropBams {
    my $self = shift;
    my %args = @_;
    $self->_processInputBams(@_);
    my %bamURIs = %{ $self->inputBams };
    my @bamKeys = sort keys %bamURIs;

    my $dbh = $self->dbHandle;
    my $sth = $dbh->prepare("DELETE FROM bamNames WHERE bamURI = ?");
    my $rows = 0;    # rows affected
    for my $bamKey (@bamKeys) {
        $rows += $sth->execute( $bamURIs{$bamKey}->as_string )
          or croak $dbh->errstr;
    }

    carp "expected to drop " . @bamKeys . " bam paths, but only dropped $rows"
      if $rows < @bamKeys;
    if ( $rows > @bamKeys ) {
        $dbh->rollback;
        croak "expected to drop " . @bamKeys
          . " bam paths, but tried to drop more ($rows).  all drops have been rolled back";
    }

    $dbh->commit;
    return $rows;
}

sub listDrivers {
    my $self = shift;

    print "Available DBI drivers:\n";
    my @drivers = DBI->available_drivers('quiet');
    my @sources;

    foreach my $driver (@drivers) {
        print "$driver\n";
        @sources = eval { DBI->data_sources($driver) };
        if ($@) {
            print "\tError: $@\n";
        }
        elsif (@sources) {
            foreach my $source (@sources) {
                print "\t$source\n";
            }
        }
        else {
            print "\tno known data sources\n";
        }
    }
}

sub backupBams {
    my $self = shift;
    my %args = @_;

    $self->registerBams(@_);

    # validate input
    my $targetDir = $args{backupTargetDir}
      || croak "need to specify backupTargetDir if backing up";

    my %targetDirURIByHost = %{
        $self->_convertToURI(
            paths     => [$targetDir],
            isDir     => 1,
            createDir => 1
        )
    };
    my ($targetDirURIHash) = values %targetDirURIByHost;
    my ($targetDirURI)     = values %{$targetDirURIHash};

    my $devName = $args{backupDeviceName}
      || croak "need to specify backupDeviceName if backing up";
    my $validateSam = $args{validateSamJar}
      || croak "need to specify validateSamJar if backing up";
    my $numJobs = $args{numJobs} || 1;

    #    my $dryRun = defined $args{dryRun} ? $args{dryRun} : 1;

    # validate bams and copy them to target dir
    #    my $dm = DM->new( dryRun => $dryRun, numJobs => $numJobs );

    my %inputURIsByHost = %{ $self->inputBamsByHost };
    my %md5sums         = %{ $self->inputBamMD5sums };
    my @targetBamURIs;
    my $FHDEBUG    = $self->_FHDEBUG;
    my $targetHost = $targetDirURI->host;
    for my $host ( sort keys %inputURIsByHost ) {
        my %hostURIHash = %{ $inputURIsByHost{$host} };
        $self->_sshLogin($host);
        $self->_sshLogin($targetHost);
        for my $bamURIKey ( sort keys %hostURIHash ) {
            my $bam = $hostURIHash{$bamURIKey}->path;

            my $bamName   = basename($bam);
            my $tmpBamDir = $self->tmpDir . "/$bamName/$md5sums{$bamURIKey}";
            my $tmpBam    = "$tmpBamDir/$bamName";
            my $targetBamDir =
              $targetDirURI->path . "/$bamName/$md5sums{$bamURIKey}";
            my $targetBamDirURI = $targetDirURI->clone;
            $targetBamDirURI->path($targetBamDir);
            my ($targetBamDirToUse) =
              @{ $self->_convertURI( [$targetBamDirURI], 'rsyncShort' ) };
            my $targetBamURI = $targetBamDirURI->clone;
            $targetBamURI->path("$targetBamDir/$bamName");
            push @targetBamURIs, $targetBamURI;

            # create temporary dir and copy files to it
            my ( $stdout, $stderr, $exit ) =
              $self->_sshCommand(
                "mkdir -p $tmpBamDir" . " && rsync -av $bam $tmpBam",
                host => $host );
            print $FHDEBUG $stdout . $stderr;
            croak $stderr. "\n" if $exit;

            # run md5sum, index and validate
            my @cmds = (
                "cd $tmpBamDir && md5sum $bamName > $bamName.md5",
                "cd $tmpBamDir && samtools index $bamName ",
                "cd $tmpBamDir && rm -f $bamName.ok"
                  . " && java -jar $validateSam I=$bamName O=$bamName.validation MODE=SUMMARY VALIDATE_INDEX=TRUE"
                  . " && touch $bamName.ok"
            );
            for my $cmd (@cmds) {
                ( $stdout, $stderr, $exit ) =
                  $self->_sshCommand( $cmd, host => $host );
                print $FHDEBUG $stdout . $stderr;
                carp $stderr. "\n" if $exit;
            }

            # create backup dir
            ( $stdout, $stderr, $exit ) =
              $self->_sshCommand( "mkdir -p $targetBamDir",
                host => $targetHost );
            print $FHDEBUG $stdout . $stderr;
            croak
"could not create dir on target host $targetHost at dir $targetBamDir\n$stderr"
              if $exit;

            # rsync files to target dir and remove tmp copies
            my $rsyncCmdTargetHostCmd =
              $host eq $targetHost ? '' : "$targetHost:";
            ( $stdout, $stderr, $exit ) = $self->_sshCommand(
                " cd $tmpBamDir "
                  . " && rsync -av ${bamName}* ${rsyncCmdTargetHostCmd}$targetBamDir/"
                  . " && cd"
                  . " && rm -rf $tmpBamDir",
                host => $host
            );
            print $FHDEBUG $stdout . $stderr;
            croak
"Failed to rsync files from $host to $targetHost\nError message: $stderr"
              if $exit;
        }
    }

    $self->registerBams(
        files            => \@targetBamURIs,
        backup           => 1,
        backupDeviceName => $devName,
    );

    # bringing input bams back into expected state
    $self->registerBams(@_);

    return $self->_returnURIs(@targetBamURIs);
}

__PACKAGE__->meta->make_immutable;
1;
__END__

=head1 NAME

bamTrackLib - Perl extension for blah blah blah

=head1 SYNOPSIS

   use bamTrackLib;
   blah blah blah

=head1 DESCRIPTION

This class was designed for use with bamTrack.pl

=head2 EXPORT

None by default.

=head1 SEE ALSO

Mention other useful documentation such as the documentation of
related modules or operating system documentation (such as man pages
in UNIX), or any relevant external documentation such as RFCs or
standards.

If you have a mailing list set up for your module, mention it here.

If you have a web site set up for your module, mention it here.

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2013 by Warren Winfried Kretzschmar

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself, either Perl version 5.8.2 or,
at your option, any later version of Perl 5 you may have available.

=head1 BUGS

None reported... yet.

=cut


## Please see file perltidy.ERR
## Please see file perltidy.ERR

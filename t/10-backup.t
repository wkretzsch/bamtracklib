# Test file created outside of h2xs framework.
# Run this like so: `perl 10-backup.t'
#   wkretzsch@gmail.com     2013/07/23 17:16:27

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More;
BEGIN { plan tests => 29 }

use warnings;
use strict;
$| = 1;
use Data::Dumper;
use bamTrackLib;

use Test::Files;
use Test::File;
use Test::Differences;

use File::Path qw(make_path remove_tree);
use File::Copy;
use File::Basename;
use Cwd 'abs_path';
use Sys::Hostname;
use Test::Warn;
use Test::Exception;

my $localhost = hostname;
my $validateSamJar = shift || "$ENV{HOME}/opt/ValidateSamFile.jar";

# create working directory for backups
my $wd = abs_path("t/10-workdir");
remove_tree($wd) if -d $wd;
make_path($wd);

my $origDir = abs_path("$wd/originals");
make_path($origDir);
my $backupDir = abs_path("$wd/backup");
remove_tree($backupDir);
make_path($backupDir);

# remove test table if it exists
my $bto = bamTrackLib->new(
    host => 'mus.well.ox.ac.uk',
    db   => "testbgibams",
    user => 'winni'
);

# remove test table if it exists
my $dbh = $bto->dbHandle;
$dbh->do("DROP TABLE IF EXISTS md5sums");
$dbh->do("DROP TABLE IF EXISTS bamNames");
$dbh->commit;

$bto = bamTrackLib->new(
    host  => 'mus.well.ox.ac.uk',
    db    => "testbgibams",
    user  => 'winni',
    DEBUG => 0,
);

my $bamList = $wd . '/bam.list';
open( my $fh, '>', $bamList ) or die "could not open $bamList for writing";
my @bamList;
my @bamNames =
  qw/MD_CHW_AAS_10011.downsamp.bam MD_CHW_AAS_10179.downsamp.bam MD_CHW_AAS_10011.invalid.bam/;
for my $bam (@bamNames) {
    copy( "samples/bams/" . $bam,                   $wd );
    copy( "samples/bams/" . $bam . ".md5.expected", $wd );
    copy( "samples/bams/" . $bam . ".validation",   $wd );
    my $bamPath = $wd . '/' . $bam;
    print $fh $bamPath . "\n";
    push @bamList, $bamPath;
}
close($fh);

$bto->registerBams( fileList => $bamList, recalibrated => 0 );

warning_like {
    $bto->backupBams(
        fileList         => $bamList,
        backupTargetDir  => $backupDir,
        backupDeviceName => 'backup1',
        validateSamJar   => $validateSamJar,
        dryRun           => 0
    );
}
qr/net.sf.picard.sam.ValidateSamFile INPUT=MD_CHW_AAS_10011.invalid.bam OUTPUT=MD_CHW_AAS_10011.invalid.bam.validation MODE=SUMMARY VALIDATE_INDEX=true/,
  "ValidateSam exited with non-zero exit status";

# now test everything is as expected
my %md5sums = %{ $bto->inputBamMD5sums };
for my $bam (@bamList) {
    my $bamName      = basename($bam);
    my $bamKey       = "rsync://${localhost}${bam}";
    my $md5sum       = $md5sums{$bamKey};
    my $bamBackupDir = "$backupDir/$bamName/$md5sum";

    # check all expected files exist
    my $tpNum = 0;
    dir_exists_ok( "$bamBackupDir", "backup dir for $bamName created" );

    file_exists_ok( "$bamBackupDir/$bamName",     "backup bam exists" );
    file_exists_ok( "$bamBackupDir/$bamName.md5", "backup bam md5 exists" );
    file_exists_ok( "$bamBackupDir/$bamName.validation",
        "backup bam validation exists" );
    if ( $bamName =~ m/invalid/ ) {
        file_not_exists_ok( "$bamBackupDir/$bamName.ok",
            "backup bam ok does not exist" );
    }
    else {
        file_exists_ok( "$bamBackupDir/$bamName.ok", "backup bam ok exists" );
    }

    # now check if files match
    compare_ok( $bam, "$bamBackupDir/$bamName",
        "backup file $bamName matches original" );
    compare_ok(
        "$bam.md5.expected",
        "$bamBackupDir/$bamName.md5",
        "backup md5 of $bamName matches original"
    );
    compare_ok(
        "$bam.validation",
        "$bamBackupDir/$bamName.validation",
        "backup validation of $bamName matches original"
    );

    # now check that bam files were registered correctly in db
    my $passedValidation = $bamName =~ m/invalid/ ? 0 : 1;
    my @retBams = $bto->retrieveBams(
        filterColumns => {
            bamName           => $bamName,
            backup            => 1,
            backupDeviceName  => 'backup1',
            passedValidateSam => $passedValidation,
            recalibrated      => 0,
        }
    );
    eq_or_diff \@retBams, ["$bamBackupDir/$bamName"],
      "backup passed validation according to db";

}

# test to make sure the recalibrated test throws expected error
throws_ok {
    $bto->registerBams( fileList => $bamList, recalibrated => 1 );
}
qr/Input bams are not all recalibrated\. Please register recalibrated and non-recalibrated bams separately\. Following is a list of non-recalibrated input bams:/,
  "faulty non-recalibrated bams registration caught";

#########################

# Insert your test code below, the Test::More module is used here so read
# its man page ( perldoc Test::More ) for help writing this test script.


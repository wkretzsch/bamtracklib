# Test file created outside of h2xs framework.
# Run this like so: `perl 20-remote-host.t'
#   wkretzsch@gmail.com     2013/07/25 10:24:33

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More;
BEGIN { plan tests => 17 }

use warnings;
use strict;
$| = 1;
use Data::Dumper;
use Net::SSH::Perl;

#use Test::Files;
use Test::Differences;
use Test::Exception;
use Test::Warn;

use File::Path qw(make_path remove_tree);
use Cwd 'abs_path';

use bamTrackLib;
use File::Basename;

my $validateSamJar = shift || "~/opt/ValidateSamFile.jar";

### PLAN
# create a test directory on a remote host
# copy files to the remote host
# register them
# make sure they registered correctly
# back up remote files to remote host
# make sure they backed up correctly

my $testHost = 'sparse.well.ox.ac.uk';
my $user     = 'winni';

my $ssh = Net::SSH::Perl->new($testHost);
ok( $ssh->login($user), "logged in to $testHost successfully as $user" );

my $testingDir       = "/tmp/$user/bamTrackLibTesting";
my $origDir          = "$testingDir/orig";
my $backupDir        = "$testingDir/backup";
my $backupDir2       = "$testingDir/backup2";
my $remoteOrigDir    = "$testHost:$origDir";
my $remoteBackupDir  = "$testHost:$backupDir";
my $remoteBackupDir2 = "$testHost:$backupDir2";

my ( $out, $err, $exit ) =
  $ssh->cmd("mkdir -p $origDir $backupDir $backupDir2");
is( $exit, 0, "created target dirs" );

is( system("rsync -av samples/bams/ $remoteOrigDir/"),
    0, "copied files to $testHost successfully" );

# create working directory for backups
my $wd = abs_path("t/20-workdir");
remove_tree($wd) if -d $wd;
make_path($wd);

# remove test table if it exists
my $bto = bamTrackLib->new(
    host => 'mus.well.ox.ac.uk',
    db   => "testbgibams",
    user => 'winni'
);

my $dbh = $bto->dbHandle;
$dbh->do("DROP TABLE IF EXISTS md5sums");
$dbh->do("DROP TABLE IF EXISTS bamNames");
$dbh->commit;

$bto = bamTrackLib->new(
    host  => 'mus.well.ox.ac.uk',
    db    => "testbgibams",
    user  => 'winni',
    DEBUG => 0,
);

# test 1: backup local to remote dir
# write bam list of bams to be backed up
my $bamList = $wd . '/bam.list';
open( my $fh, '>', $bamList ) or die "could not open $bamList for writing";
my @bamNames =
  qw/MD_CHW_AAS_10011.downsamp.bam MD_CHW_AAS_10179.downsamp.bam MD_CHW_AAS_10011.invalid.bam/;
my @bamMD5s =
  qw/42d3ad34e941cee32cb4468c01671ead eca486783c24debd2beb68628aae644a 385291917cf73e2bea73100ff88f181c/;
my @bamList = sort map { abs_path("samples/bams/$_") } @bamNames;
map { print $fh $_ . "\n" } @bamList;
close($fh);

# now backup the files
warning_like {
    $bto->backupBams(
        fileList         => $bamList,
        backupTargetDir  => $remoteBackupDir,
        backupDeviceName => 'backup1',
        validateSamJar   => $validateSamJar,
    );
}
qr/net.sf.picard.sam.ValidateSamFile INPUT=MD_CHW_AAS_10011.invalid.bam OUTPUT=MD_CHW_AAS_10011.invalid.bam.validation MODE=SUMMARY VALIDATE_INDEX=true/,
  "Invalide Bam did not validate";

my @backedUpBamList;
for my $bamNum ( 0 .. $#bamNames ) {
    push @backedUpBamList,
"$remoteBackupDir/$bamNames[$bamNum]/$bamMD5s[$bamNum]/$bamNames[$bamNum]";
}

@backedUpBamList = sort @backedUpBamList;

# check to make sure files were copied across
for my $bam (@backedUpBamList) {
    my ( $host, $absPath ) = split( ':', $bam );
    my ( $out, $err, $exit ) = $ssh->cmd("test -f $absPath");
    is( $exit, 0, "bam copied to host $host for backup: $absPath" );
}

# test retrieve bams possible input error
my @bamSampleNames = (undef) x @bamNames;
throws_ok {
    $bto->retrieveBams(
        filterColumns => { sampleName => \@bamSampleNames, backup => 0 },
        returnAs      => 'absolute'
    );
}
qr/a value in the filterColumns array for header sampleName was undef/,
  "make sure api fails on undef input filtercolumn vals";

# retrieve to make sure they registered
@bamSampleNames = map { m|^([A-Za-z0-9_]+)|; $1; } @bamNames;
my @bams = $bto->retrieveBams(
    filterColumns => { sampleName => \@bamSampleNames, backup => 0 },
    returnAs      => 'absolute'
);
@bams = sort @bams;
eq_or_diff \@bams, \@bamList, "bamlist saved in db and retrieved correctly";

# retrieve backed up bams to make sure they registered
@bams = sort $bto->retrieveBams(
    filterColumns => { sampleName => \@bamSampleNames, backup => 1 },
    returnAs      => 'rsyncShort'
);
eq_or_diff \@bams, \@backedUpBamList, "bamlist of backups retrieved correctly";

# 10 tests by here

# test 2, backup remote to remote

# register remote bams
# write bam list of bams to be backed up
my $remoteBamList = $wd . '/remote.bam.list';
open( $fh, '>', $remoteBamList )
  or die "could not open $remoteBamList for writing";
my @remoteBamList2 = map { "$remoteOrigDir/$_" } @bamNames;
map { print $fh $_ . "\n" } @remoteBamList2;
close($fh);

# now backup the files
warning_like {
    $bto->backupBams(
        fileList         => $remoteBamList,
        backupTargetDir  => $remoteBackupDir2,
        backupDeviceName => 'backup2',
        validateSamJar   => $validateSamJar,
    );
}
qr/net.sf.picard.sam.ValidateSamFile INPUT=MD_CHW_AAS_10011.invalid.bam OUTPUT=MD_CHW_AAS_10011.invalid.bam.validation MODE=SUMMARY VALIDATE_INDEX=true/,
  "Invalide Bam did not validate";

my @backedUpBamList2;
for my $bamNum ( 0 .. $#bamNames ) {
    push @backedUpBamList2,
"$remoteBackupDir2/$bamNames[$bamNum]/$bamMD5s[$bamNum]/$bamNames[$bamNum]";
}

@backedUpBamList2 = sort @backedUpBamList2;

# check to make sure files were copied across
for my $bam (@backedUpBamList2) {
    my ( $host, $absPath ) = split( ':', $bam );
    my ( $out, $err, $exit ) = $ssh->cmd("test -f $absPath");
    is( $exit, 0, "bam copied to host $host for backup: $absPath" );
}

# retrieve to make sure the to be backed up files registered
@bamSampleNames = map { m|^([A-Za-z0-9_]+)|; $1; } @bamNames;
@bams = $bto->retrieveBams(
    filterColumns => { sampleName => \@bamSampleNames, backup => 0 },
    returnAs      => 'rsyncShort'
);
@bams = grep { m/^($testHost)/ } @bams;
@bams = sort @bams;
@remoteBamList2 = sort @remoteBamList2;
eq_or_diff \@bams, \@remoteBamList2,
  "bamlist2 saved in db and retrieved correctly";

# retrieve backed up bams to make sure they registered
@bams = sort $bto->retrieveBams(
    filterColumns =>
      { sampleName => \@bamSampleNames, backup => 1, backupDeviceName => 'backup2' },
    returnAs => 'rsyncShort'
);
eq_or_diff \@bams, \@backedUpBamList2, "bamlist of backups retrieved correctly";

# post test clean-up
is( system("ssh $testHost 'rm -rf $testingDir'"),
    0, "removed all test files from remote host" );

